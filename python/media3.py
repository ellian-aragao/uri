notas = [2,3,4,1]
num = [float(x) for x in input().split()]
media = notas[0] * num[0] + notas[1] * num[1] + notas[2] * num[2] + num[3]
media /= 10.0

if media < 5.0:
    print('Media: {:.1f}'.format(media))
    print('Aluno reprovado.')

elif media >= 7.0:
    print('Media: {:.1f}'.format(media))
    print ('Aluno aprovado.')

else:
    aux = float(input())
    print('Media: {}'.format(media))
    print('Aluno em exame.')
    print('Nota do exame: {:.1f}'.format(aux))
    media = (media + aux) / 2.0

    if media >= 5:
        print('Aluno aprovado.')
    else:
        print('Aluno reprovado.')

    print('Media final: {:.1f}'.format(media))
